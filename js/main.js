$(document).ready(function () {
    $(window).scroll(function () {
        if ($(this).scrollTop() > 200) {
            $('.button-up').fadeIn();
        } else {
            $('.button-up').fadeOut();
        }

    });

    $('.button-up').click(function () {
        $('body, html').animate({ scrollTop: 0 }, 800);
    });

    var tl = new TimelineMax({});
    var wheel = $('.animate-wheel');
    var direction = 'normal';
    var position = [
        {
            x: 114,
            y: 47
        },
        {
            x: 714,
            y: 328
        },
        {
            x: 114,
            y: 599
        },
        {
            x: 714,
            y: 888
        },
        {
            x: 114,
            y: 1211
        },
        {
            x: 714,
            y: 1514
        },
        {
            x: 114,
            y: 1776
        }
    ];

    if ($('.action-rules-block').length) {
        $(window).on('scroll', function () {
            var allNumb = $('.number-wrapper');
            var itemLength = $('.number-wrapper').length;
                    allNumb.each(function (index, el) {
                        if (!$(this).hasClass('was-animated') && index < itemLength) {
                                $(el).addClass('was-animated');
                                $('body').trigger('elementInViewport', [index]);
                        } else {
                            animateElement(wheel, position[6].y + 'px', position[6].x + 'px', direction, tl);
                            $(el).removeClass('was-animated');
                            // $(el).hide();
                            tl.to(wheel, 0.1, { animation: 'none' });
                        }
                    })
        });


        $('body').on('elementInViewport', function (e, step) {
            direction === 'normal' ? direction = 'reverse' : direction = 'normal';
                animateElement(wheel, position[step].y + 'px', position[step].x + 'px', direction, tl);
                tl.to(wheel, 1, { animation: 'rotate 2s infinite linear' });
        });
    }

    function animateElement(el, top, left, direction, tl) {
        tl.to(el, 4, { left: left, top: top, animationDirection: direction })
    }


});



